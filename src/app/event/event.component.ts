import { Component, OnInit } from '@angular/core';
import { CommonServices } from '../services/common.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  providers: [CommonServices]
})
export class EventComponent implements OnInit {
  event: any;
  loading: boolean = true;
  
  constructor(private commonService: CommonServices, private route: ActivatedRoute) { 

    window.scroll(0,0)
    
    let id = this.route.snapshot.paramMap.get("id");
    
    this.commonService.getEventDetails(id).subscribe((response)=> {      
      this.event = (<any>response).data[0]; 
      this.loading = false;
      console.log(response)
    });
    

  }

  ngOnInit(): void {
    
  }

}
