import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from '../constant';

@Injectable()
export class CommonServices {
    constructor(private http: HttpClient, private constants: Constants) {}
    getCategories() {
        return this.http.get(this.constants.APIEndPoint + 'get-categories');
    }

    getEvents() {
        return this.http.get(this.constants.APIEndPoint + 'get-events')
    }

    getEventDetails(id: string) {
        return this.http.post(this.constants.APIEndPoint + 'get-event', {id})
    }



}