import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CommonServices } from '../services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [CommonServices]
})
export class HomeComponent implements OnInit {
  events: any = []
  categories : any = [];

  constructor(private commonService: CommonServices) { 
    this.commonService.getCategories().subscribe((response)=> {      
      this.categories = (<any>response).data;
    });

    this.commonService.getEvents().subscribe((response)=> {      
      this.events = (<any>response).data; 
    });

  }

  ngOnInit(): void {
    
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,    
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 5
      }
    },
    nav: false
  }


}
